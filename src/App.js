import React, { Component, Fragment } from 'react';
import './App.css';
import Header from './components/Header';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      userName:
        {
          ad: 'mert',
          soyad: 'yakdemir',
        },
    };
  }
  componentDidMount() {
    fetch(' https://reqres.in/api/users?page=2')
      .then(res => res.json())
      .then((result) => {
        this.setState({
          data: result.data,
        });
      });
  }

  render() {
    const { data, userName } = this.state;
    return (
      <div className="App">
        <Header userName={userName} />
        {data.map(datas => (
          <Fragment key={data.id}>
            <div className="data-name">

              {datas.first_name} {datas.last_name}
            </div>
            <div className="avatar-img">
              <img src={datas.avatar} alt={data.id} />
            </div>
          </Fragment>
          ))}
      </div>
    );
  }
}

export default App;
