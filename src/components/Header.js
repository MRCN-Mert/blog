import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Header extends Component {
  render() {
    const { userName } = this.props;
    return (
      <div>
        <div>
          {userName.ad} {userName.soyad}
        </div>
      </div>
    );
  }
}

Header.propTypes = {
  userName: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
};

export default Header;
